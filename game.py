from random import randint

username = input("Hi! What is your name?")

minyear = 1924
maxyear = 2004

for x in range(5):

    usermonth = randint(1, 12)
    useryear = randint(minyear, maxyear)

    if usermonth == 1:
        abcmonth = "January"
        userday = randint(1, 31)
    elif usermonth == 2:
        abcmonth = "February"
        userday = randint (1, 28)
    elif usermonth == 3:
        abcmonth = "March"
        userday = randint (1, 31)
    elif usermonth == 4:
        abcmonth = "April"
        userday = randint (1, 30)
    elif usermonth == 5:
        abcmonth = "May"
        userday = randint (1, 31)
    elif usermonth == 6:
        abcmonth = "June"
        userday = randint (1, 30)
    elif usermonth == 7:
        abcmonth = "July"
        userday = randint (1, 31)
    elif usermonth == 8:
        abcmonth = "August"
        userday = randint (1, 31)
    elif usermonth == 9:
        abcmonth = "Semptember"
        userday = randint (1, 30)
    elif usermonth == 10:
        abcmonth = "October"
        userday = randint (1, 31)
    elif usermonth == 11:
        abcmonth = "November"
        userday = randint (1, 30)
    elif usermonth == 12:
        abcmonth = "December"
        userday = randint (1, 31)


    print("Guess", x+1, ":", username, ", Were you born in", userday, abcmonth, useryear, "?")

    useranswer = input("Yes or No? ")

    if useranswer.lower() == "yes":
        print("I knew it!")
        break

    elif useranswer.lower() == "no" and x < 4:
        print("Drat! Lemme try again!")
        hint = input("Earlier or later?")
        if hint.lower() == "earlier":
            maxyear = useryear - 1
        elif hint.lower() == "later":
            minyear = useryear + 1
        else:
            print("Invalid answer, please answer either 'earlier or later'")

    elif useranswer.lower() =="no" :
        print("I have other things to do. Good bye.")

    else :
        print("Invalid answer, please answer either 'yes or no'")


print("Thanks for playing!")
